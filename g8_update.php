<?php
session_start();

include_once("/var/www/lo/lib/g8_core_lib.php");
include_once("/var/www/lo/cfg/g8_core_db_cfg.php");
include_once("/var/www/lo/g8_poll/lib/g8_poll_lib.php");
include_once("/var/www/lo/g8_actn/lib/g8_actn_lib.php");
include_once("/var/www/lo/g8_gpsg/lib/g8_gpsg_lib.php");
//Opening db connection to mysql
$g8_link=g8_db_connect();
$_SESSION['g8_link']=$g8_link;
$g8_db_selected=g8_db_select($g8_link);
//opening db connection to mysqli
$g8_lo_link=g8_mysqli_db_connect();
$_SESSION['g8_lo_link']=$g8_lo_link;
$_SESSION['g8_trigger']="g8lifeon=:=";
# This is the site we use to send message
$_SESSION['g8_gcmhost']="https://autoremotejoaomgcd.appspot.com/sendmessage?key=";
$g8_lo_id=$_REQUEST['g8_lo_id'];
$g8_msg=$_REQUEST['g8_msg'];
$g8_m=urldecode($g8_msg);
##TBD urldecode to be removed TBD##
$g8_m=urldecode($g8_m);
#echo $g8_m;

/**
 * Function to get value of switch variable for diff. cases
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param string $g8_m
 * @param integer $g8_lo_id 
 * 
 * @return string $g8_switch
 */ 		
function g8_switch_var($g8_m,$g8_lo_id){
	//Condition to check if message is a Device Messages
	if (strtok($g8_m , ':')=='Power'){
		$g8_switch='Device';
	}
	//Condition to check if message is an Alarm Messages
	elseif(stripos($g8_m,'alarm') !== false && stripos($g8_m,'lac') == false){
		if(explode(" ",$g8_m)[0]=='sensor'){
			$g8_switch='SAlarm';
		}else{
			$g8_switch='AAlarm';
	    }
		
	}
	//Condition to check if message is a Vehical Messages
	elseif (is_numeric(substr($g8_m,0,10 ))){
		//Condition to check for IMEI No. from g8_dev_prop table
		$g8_iemi=g8_get_dev_num($g8_lo_id);
		$g8_chk=explode("\n",$g8_m)[0];
		$g8_chk=rtrim($g8_chk);
			if( $g8_chk==$g8_iemi){
				#echo 'hello';
				$g8_switch='Vehicle';
			}else{
				#echo 'bye';
				$g8_switch='1';
			}
			
	}elseif (explode(":",$g8_m)[0]=='lat'){
		$g8_switch='Locn';
	}
	//Condition for Configuration message 
	else{
		$g8_switch='default';
	}
	return $g8_switch;
}
// Condition to check if g8_lo_id is set and is not 0
if (isset($_REQUEST['g8_lo_id']) && $_REQUEST['g8_lo_id']!=0 && $_REQUEST['g8_lo_id']!=''){
	// Condition to check if g8_lo_id exists	
	$g8_lo_chk=g8_check_lo_exist($g8_lo_id);
	
	if($g8_lo_chk == 1){
		
	  $g8_sw=g8_switch_var($g8_m,$g8_lo_id);
		if($g8_sw==1){
			$g8_result=-1;
		}else{
			switch($g8_sw){
				
				case 'Device': 
						#echo 'A';
						$g8_param_type='g8_device_';
						g8_device_msg($g8_m,$g8_lo_id,$g8_param_type);
				break;
		
				case 'AAlarm':
	 					#echo 'B';
	 					$g8_param_type='g8_';
	 					g8_alarm_msg($g8_m,$g8_lo_id,$g8_param_type);
				break;
				
				case 'SAlarm':
	 					#echo 'C';
	 					$g8_param_type='g8_';
	 					g8_alarm_msg($g8_m,$g8_lo_id,$g8_param_type);
				break;
				
				case 'Vehicle':
	 					#echo 'D';
	 					$g8_param_type='g8_';
	 					g8_vehicle_msg($g8_m,$g8_lo_id,$g8_param_type);
						
				break;
				case 'Locn':
						$g8_param_type='g8_';
						g8_locn($g8_m,$g8_lo_id,$g8_param_type);
				break;
				case 'Cfg':
	 					#echo 'E';
	 					$g8_param_type='g8_cfg_';
	 					g8_cfg_msg($g8_m,$g8_lo_id,$g8_param_type);
				break;
	            default:
	 					#echo 'E';
	 					$g8_param_type='g8_default';
	 					g8_default($g8_m,$g8_lo_id,$g8_param_type);
				break;
	            
	            
        }	
       $g8_result=1;
	  }
        
    }else{
		$g8_result=-1;
	}
}else{
	$g8_result=-1;
}
echo $g8_result;
/**
 * Function to add device msg values in g8_param_log
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param string $g8_m
 * @param integer $g8_lo_id 
 * @param string $g8_param_type
 * 
 * @return nothing
 */ 	
function g8_device_msg($g8_m,$g8_lo_id,$g8_param_type){
	$g8_mg=explode("\n",$g8_m);
	
		foreach ($g8_mg as $key => $g8_msgs){
			if($key==0){
					$g8_us=explode(' ',$g8_mg[0]);
					if($g8_us[1]=='ON' || $g8_us[1]=='OFF'){
							$g8_time_stamp=date('Y-m-d H:i:s');
							$g8_str=explode(' ',$g8_mg[0]);
							$g8_db_param='g8_device_power';
							$g8_value=$g8_str[1];
							$g8_value=str_ireplace('ON' , '1' , $g8_value);
							$g8_value=str_ireplace('OFF' , '0' , $g8_value);
							$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
							$g8_db_param='g8_device_batt';
							$g8_value=$g8_str[3];
							$g8_value=str_ireplace('%' , '' , $g8_value);
							$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
					}else{
						$g8_str=explode(' ',$g8_mg[0]);
						foreach($g8_str as $g8_st){
							$g8_param=explode(":",$g8_st)[0];
							$g8_db_param=$g8_param_type.''.strtolower($g8_param);
							$g8_value=explode(":",$g8_st)[1];
							$g8_value=str_ireplace('ON' , '1' , $g8_value);
							$g8_value=str_ireplace('OFF' , '0' , $g8_value);
							$g8_time_stamp=date('Y-m-d H:i:s');
							$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
						}
					}
			}else{
				$g8_param=explode(":",$g8_msgs)[0];
				$g8_value=explode(":",$g8_msgs)[1];
				$g8_value=str_ireplace('%' , '' , $g8_value);
				#$g8_value=str_ireplace(' ' , '_' , $g8_value);
				$g8_time_stamp=date('Y-m-d H:i:s');
				
					switch($g8_param){
							case 'Service':
							case 'service':
								$g8_db_param='g8_vehicle_service';
								$g8_value=explode(":",$g8_msgs)[1];
								$g8_value=str_ireplace('Day' , '' , $g8_value);
								#	if(strpos($g8_value,'-')== false && ){
								#		$g8_value=trim($g8_value);
								#		$g8_value='+'.$g8_value;
								#	} 	
							break;
							case 'ACC':
								$g8_db_param='g8_vehicle_ignition';
								$g8_value=str_ireplace('ON' , '1' , $g8_value);
								$g8_value=str_ireplace('OFF' , '0' , $g8_value);
							break;
							case 'Bat':
								$g8_db_param='g8_device_batt';
							break;
							case 'GPS':
								$g8_db_param='g8_device_gps';
								$g8_value=explode(":",$g8_msgs)[1];
								if(trim($g8_value) != 'ON' && trim($g8_value) !='OK'){
									$g8_value=0;
								}else{
									$g8_value=str_ireplace('ON' , '1' , $g8_value);
									$g8_value=str_ireplace('OK' , '1' , $g8_value);
								}
							break;
							case 'GPRS':
								$g8_db_param='g8_device_gprs';
								$g8_value=explode(":",$g8_msgs)[1];
								if(trim($g8_value) != 'ON' && trim($g8_value)!='OFF'){
									$g8_value=0;
								}else{
									$g8_value=str_ireplace('ON' , '1' , $g8_value);
									$g8_value=str_ireplace('OFF' , '0' , $g8_value);
								}
							break;
							case 'IP':
								$g8_db_param='g8_device_server';
								$g8_value=explode(":",$g8_msgs)[1];
								$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
								$g8_db_param='g8_device_port';
								$g8_value=explode(":",$g8_msgs)[2];
							break;	
							case 'DTC':
								$g8_db_param='g8_vehicle_error';
							break;	
							default:
								$g8_db_param=$g8_param_type.''.strtolower($g8_param);
								$g8_db_param=str_ireplace(' ' , '_' , $g8_db_param);
								$g8_value=str_ireplace('ON' , '1' , $g8_value);
								$g8_value=str_ireplace('OFF' , '0' , $g8_value);
							break;	
					}	
			}	
								if($g8_value=="" || $g8_value==" " || $g8_value==";" || $g8_value=="/n" || $g8_value==":" || $g8_value=="+" || $g8_value=="   "){
										$g8_value=0;
								}
								
									//Function to insert values in DB in param_log table								
									if($g8_db_param!="g8_" && $g8_db_param!='T' && $g8_db_param!='' && $g8_db_param!="g8_device_" && $g8_db_param!=(is_numeric(substr($g8_db_param,4,10 )))){
			    							
			    							$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
									}
				
		}
}
/**
 * Function to add vehicle msg values in g8_param_log
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param string $g8_m
 * @param integer $g8_lo_id 
 * @param string $g8_param_type
 * 
 * @return nothing
 */ 	
function g8_vehicle_msg($g8_m,$g8_lo_id,$g8_param_type){
	$g8_mg=explode("\n",$g8_m); 
	$g8_rpm_chk=explode("\n",$g8_m)[4];
    $g8_rpm_chk=explode(":",$g8_rpm_chk)[1];
	$g8_sp_chk=explode("\n",$g8_m)[6];
	$g8_sp_chk=explode(":",$g8_sp_chk)[1];
	$g8_temp_chk=explode("\n",$g8_m)[8];
	$g8_temp_chk=explode(":",$g8_temp_chk)[1];
	#Condition to check if Vehicle is on or off
	if($g8_rpm_chk>0 && $g8_sp_chk>2 && $g8_temp_chk>0){
		foreach ($g8_mg as $key => $g8_msgs){
			$g8_param=explode(":",$g8_msgs)[0];
			$g8_value=explode(":",$g8_msgs)[1];
					switch($g8_param){
							case 'AF':
								$g8_db_param='g8_avg_fuel';
							break;
							case 'FLI':
								$g8_db_param='g8_fuel';
							break;
							case 'Dtime':
								$g8_db_param='g8_dist_err';
							break;
							case 'Pload':
								$g8_db_param='g8_power_load';
								$g8_value=str_ireplace('%' , '' , $g8_value);
							break;
							case 'ATP':
								$g8_db_param='g8_torque';
								$g8_value=str_ireplace('%' , '' , $g8_value);
							break;
							case 'BAT':
								$g8_db_param='g8_vehicle_batt';
							break;
							case 'speed':
								$g8_db_param='g8_vehicle_speed';
							break;
							case 'T':
								$g8_db_param='T';
								#$g8_value=explode(":",$g8_msgs,2)[1];
								#$g8_value=(strtotime($g8_value)); 
								//Converting timestamp from format(d/m/y) to usa format(m/d/y) 
								#if($g8_value==''){
									$g8_value=explode(":",$g8_msgs,2)[1];
									$g8_d=explode(' ',$g8_value)[0];
									$g8_t=explode(' ',$g8_value)[1];
									$g8_dt=explode('/',$g8_d);
									$g8_dt=$g8_dt[1].'/'.$g8_dt[0].'/'.$g8_dt[2];
									$g8_dt= $g8_dt.' '.$g8_t ;
									$g8_dt=(strtotime($g8_dt)); 		
									$g8_time=date('Y-m-d H:i:s',$g8_dt);
								#}else{
								#	$g8_time=date('Y-m-d H:i:s',$g8_value);
								#}
							break;
							case 'DTC':
								$g8_db_param='g8_dtc';
								$g8_value=trim($g8_value);
							break;
							default:
								$g8_db_param=$g8_param_type.''.strtolower($g8_param);
							break;
					}	
							
								if($g8_value=="" || $g8_value==" " || $g8_value==";" || $g8_value=="/n" || $g8_value==":" || $g8_value=="   " || $g8_value=="+" || $g8_value=="000" || $g8_value=="00000"){
									$g8_value=0;
								}
								if($g8_time!=""){
									$g8_time_stamp=$g8_time;
								}else{
									$g8_time_stamp=date('Y-m-d H:i:s');
								}       				        
									//Function to insert values in DB in param_log table
									if($g8_db_param!="g8_" && $g8_db_param!='T' && $g8_db_param!='' && $g8_db_param!="g8_device_" && $g8_db_param!=(is_numeric(substr($g8_db_param,4,10 )))){
											
											$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
									}
													
		}
		$g8_db_param='g8_vehicle_status';
		$g8_value=1;
		$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
	}else{
		$g8_db_param='g8_vehicle_status';
		$g8_value=0;
		$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp=date('Y-m-d H:i:s'),$g8_table='g8_param_log');	
	}
}
/**
 * Function to add config. msg values in g8_param_log
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param string $g8_m
 * @param integer $g8_lo_id 
 * @param string $g8_param_type
 * 
 * @return nothing
 */ 
function g8_cfg_msg($g8_m,$g8_lo_id,$g8_param_type){
	$g8_mg=explode("\n",$g8_m);
	
		foreach ($g8_mg as $key => $g8_msgs){
			$g8_param=explode(" ",$g8_msgs)[0];
			
					switch($g8_param){
							case 'Set':
								if(stripos($g8_msgs,'tank')){
									$g8_db_param='g8_set_tank';
									$g8_value=explode(" ",$g8_msgs)[2];	
									$g8_value=str_ireplace('ok' , '1' , $g8_value);
									$g8_value=str_ireplace('!' , '' , $g8_value);
									if($g8_value!=1){
										$g8_value=0;
									}
								}else{
									$g8_db_param='g8_set_odo';
									$g8_value=explode(" ",$g8_msgs)[2];	
									$g8_value=str_ireplace('ok' , '1' , $g8_value);
									$g8_value=str_ireplace('!' , '' , $g8_value);
									if($g8_value!=1){
										$g8_value=0;
									}
								}
							break;
							case 'service':
							case 'Service':
								$g8_db_param=$g8_param_type.''.strtolower($g8_param);
								$g8_value=explode(" ",$g8_msgs)[1];	
								$g8_value=str_ireplace('ok' , '1' , $g8_value);
								$g8_value=str_ireplace('!' , '' , $g8_value);
								if($g8_value!=1){
									$g8_value=0;
								}
							break;
							case 'time':
							case 'Time':
								$g8_db_param='g8_set_timezone';
								$g8_value=explode(" ",$g8_msgs)[1];	
								$g8_value=str_ireplace('ok' , '1' , $g8_value);
								$g8_value=str_ireplace('!' , '' , $g8_value);
								if($g8_value!=1){
									$g8_value=0;
								}
							break;
							case 'suppress':
							case 'Suppress':
								$g8_db_param=$g8_param_type.''.strtolower($g8_param);
								$g8_value=explode(" ",$g8_msgs)[2];	
								$g8_value=str_ireplace('ok' , '1' , $g8_value);
								$g8_value=str_ireplace('!' , '' , $g8_value);
								if($g8_value!=1){
									$g8_value=0;
								}
							break;
							case 'speed':
								$g8_db_param='g8_speed_set';
								$g8_value=explode(" ",$g8_msgs)[1];	
								$g8_value=str_ireplace('ok' , '1' , $g8_value);
								$g8_value=str_ireplace('!' , '' , $g8_value);
								if($g8_value!=1){
									$g8_value=0;
								}
							break;
							case 'reset':
								$g8_db_param='g8_cfg_reset';
								$g8_value=explode(" ",$g8_msgs)[1];	
								$g8_value=str_ireplace('ok' , '1' , $g8_value);
								$g8_value=str_ireplace('!' , '' , $g8_value);
								if($g8_value!=1){
									$g8_value=0;
								}
							case 'APN':
								$g8_db_param='g8_cfg_apn';
								$g8_value=explode(" ",$g8_msgs)[1];	
								$g8_value=str_ireplace('ok' , '1' , $g8_value);
								$g8_value=str_ireplace('!' , '' , $g8_value);
								if($g8_value!=1){
									$g8_value=0;
								}	
							break;
							case 'Tracker' :
                                $g8_db_param='g8_cfg_tracker';
                                if(stripos($g8_msgs,'deactivated') !== false){
                                    $g8_value=0;
                                }else{
                                    $g8_value=1;
                                }
                            break;
                            case 'less':
                            case 'Less':
                                $g8_db_param='g8_cnsrv_gprs';
                                if(stripos($g8_msgs,'OFF') !== false){
                                    $g8_value=0;
                                }else{
                                    $g8_value=1;
                                }
                            break;
                            case 'lowbattery':
                                $g8_db_param='g8_cfg_lowbatt';
                                if(stripos($g8_msgs,'OFF') !== false){
                                    $g8_value=0;
                                }else{
                                    $g8_value=1;
                                }
                            break;
							default:
								$g8_db_param=$g8_param_type.''.strtolower($g8_param);
								$g8_value=explode(":",$g8_msgs)[1];
								$g8_value=str_ireplace('ok' , '1' , $g8_value);	
								$g8_value=str_ireplace('!' , '' , $g8_value);
							break;
						}
								if($g8_value=="" || $g8_value==" " || $g8_value==";" || $g8_value=="/n" || $g8_value==":"){
								$g8_value=0;
								}
							
									$g8_time_stamp=date('Y-m-d H:i:s');
								       				        
									//Function to insert values in DB in param_log table								
									if($g8_db_param!="g8_" && $g8_db_param!='T' && $g8_db_param!='' && $g8_db_param!="g8_device_" && $g8_db_param!=(is_numeric(substr($g8_db_param,4,10 )))){
			    				
			    							$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
									}	
													
		}
}
/**
 * Function to add alarm msg values in g8_param_log
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param string $g8_m
 * @param integer $g8_lo_id 
 * @param string $g8_param_type
 * 
 * @return nothing
 */ 
function g8_alarm_msg($g8_m,$g8_lo_id,$g8_param_type){
	$g8_mg = preg_split( '/(\s|\n)/', $g8_m );
	#$g8_mg=explode("\n",$g8_m);
	if(explode("\n",$g8_m)[4]==''){
		$g8_dttm=explode("\n",$g8_m)[1];
		$g8_dttm=explode(":",$g8_dttm,2)[1];
		#$g8_dttm=str_ireplace('T:' , '' , $g8_dttm);
		$g8_d=explode(' ',$g8_dttm)[0];
		$g8_t=explode(' ',$g8_dttm)[1];
		$g8_dt=explode('/',$g8_d);
		$g8_dt=$g8_dt[1].'/'.$g8_dt[0].'/'.$g8_dt[2];
		$g8_dt= $g8_dt.' '.$g8_t ;
		$g8_dt=(strtotime($g8_dt)); 		
		$g8_time=date('Y-m-d H:i:s',$g8_dt);
	}else{
		$g8_t=explode("\n",$g8_m)[4];
		$g8_tm=explode(":",$g8_t,2)[1];
		$g8_tm=rtrim($g8_tm); 
		$g8_tm=str_ireplace('/' , '-' , $g8_tm);
		$g8_tm=(strtotime($g8_tm)) ; 
		$g8_time=date('Y-m-d H:i:s',$g8_tm);
	}	
		foreach ($g8_mg as $key => $g8_msgs){
			$g8_param=explode(":",$g8_msgs)[0];
			@$g8_value=explode(":",$g8_msgs)[1];
				if($key==0 && strpos($g8_m,'!') !== false){
					$g8_param='Alarm';
				}
				if(is_numeric($g8_param) || $g8_param==''){
					$g8_db_param='T';
				}else{
						
					switch($g8_param){
							case 'Alarm':
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param='g8_incident_sensor';
									$g8_value=explode(" ",$g8_msgs)[0].'_'.'alarm';
									$g8_value=str_ireplace(' ' , '_' , $g8_value);
								}else{
									$g8_db_param='g8_incident_acc';
									$g8_value=explode(" ",$g8_msgs)[0].'_'.'alarm';
									$g8_value=str_ireplace(' ' , '_' , strtolower($g8_value));
									#Runs query to Get ObdMessage
									$g8_set_obdmsg=g8_do_lo($g8_lo_id,$g8_cmd='SET',$g8_param='ObdStatus','');
								}
							break;
							case 'speed':
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param='g8_speed_sensor';
									$g8_speed=explode(":",$g8_msgs)[1];
								}elseif(explode(" ",$g8_m)[0]=='ACC'){
									$g8_db_param='g8_speed_acc';
									$g8_speed=explode(":",$g8_msgs)[1];
								}else{
									$g8_db_param='g8_speed';
								}
							break;
							case 'alarm!':
									$g8_db_param='T';
							break;
							case 'T':
									$g8_db_param='T';
							break;
							case 'dir':
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param='g8_dir_sensor';
									$g8_dir=explode(":",$g8_msgs)[1];
								}else{
									$g8_db_param='g8_dir_acc';
									$g8_dir=explode(":",$g8_msgs)[1];
								}
							break;
							case 'lat':
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param='g8_lat_sensor';
									$g8_lat=explode(":",$g8_msgs)[1];
								}else{
									$g8_db_param='g8_lat_acc';
									$g8_lat=explode(":",$g8_msgs)[1];
								}
							break;
							case 'long':
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param='g8_long_sensor';
									$g8_long=explode(":",$g8_msgs)[1];
								}else{
									$g8_db_param='g8_long_acc';
									$g8_long=explode(":",$g8_msgs)[1];
								}
							break;
							case 'http':
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param='g8_url_sensor';
									$g8_value=$g8_msgs;
								}else{
									$g8_db_param='g8_url_acc';
									$g8_value=$g8_msgs;
								}
							break;
							default:
								if(explode(" ",$g8_m)[0]=='sensor'){
									$g8_db_param=$g8_param_type.''.strtolower($g8_param).'_sensor';
									
								}else{
									$g8_db_param=$g8_param_type.''.strtolower($g8_param).'_acc';
									
								}
								
							break;
					}
				}
								
									if($g8_value=="" || $g8_value==" " || $g8_value==";" || $g8_value=="/n" || $g8_value==":"){
									$g8_value=0;
									}
									if($g8_time!=""){
										$g8_time_stamp=$g8_time;
									}else{
										$g8_time_stamp=date('Y-m-d H:i:s');
									}       				        
									
										//Function to insert values in DB in param_log table							
										if($g8_db_param!="g8_" && $g8_db_param!='T' && $g8_db_param!='' && $g8_db_param!="g8_device_" && $g8_db_param!=(is_numeric(substr($g8_db_param,4,10 )))){
									
												$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
																				
										}	
		}
		if($g8_lat!='' && $g8_long!='' && $g8_time_stamp!='0000-00-00 00:00:00'){				
				$g8_insrt_track=g8_gpsg_track($g8_lo_id,$g8_lat,$g8_long,$g8_speed,$g8_dir,$g8_time_stamp);
		}
}
/**
 * Function to add Location msg values in g8_param_log
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param string $g8_m
 * @param integer $g8_lo_id 
 * @param string $g8_param_type
 * 
 * @return nothing
 */ 
function g8_locn($g8_m,$g8_lo_id,$g8_param_type){
	$g8_mg=explode(" ",$g8_m);
	$g8_date=explode("\n",$g8_m)[1];
	$g8_date=(strtotime($g8_date)); 
	//Converting timestamp from format(d/m/y) to usa format(m/d/y) 
		if($g8_date==''){
				$g8_date=explode("\n",$g8_m)[1];
				$g8_date=explode(":",$g8_date,2)[1];
				$g8_d=explode(' ',$g8_date)[0];
				$g8_t=explode(' ',$g8_date)[1];
				$g8_dt=explode('/',$g8_d);
				$g8_dt=$g8_dt[1].'/'.$g8_dt[0].'/'.$g8_dt[2];
				$g8_dt= $g8_dt.' '.$g8_t ;
				$g8_dt=(strtotime($g8_dt)); 		
				$g8_time=date('Y-m-d H:i:s',$g8_dt);
		}else{
				$g8_time=date('Y-m-d H:i:s',$g8_date);
		}
	$g8_url=explode("\n",$g8_m)[2];
		
		foreach ($g8_mg as $key => $g8_msgs){
			$g8_param=explode(":",$g8_msgs)[0];
			$g8_value=explode(":",$g8_msgs)[1];
				
				switch($g8_param){
							
							case 'speed':
									$g8_db_param='g8_speed';
									$g8_speed=explode(":",$g8_msgs)[1];
							break;
							case 'T':
									$g8_db_param='T';
							break;
							case 'lat':
									$g8_db_param='g8_lat';
									$g8_lat=explode(":",$g8_msgs)[1];
								
							break;
							case 'long':
									$g8_db_param='g8_long';
									$g8_long=explode(":",$g8_msgs)[1];
								
							break;
							case 'dir':
									$g8_db_param='g8_dir';
									$g8_value=explode(":",$g8_msgs)[1];
									$g8_value=str_ireplace('T' , '' , $g8_value);
									$g8_dir=explode(":",$g8_msgs)[1];
									$g8_dir=str_ireplace('T' , '' , $g8_dir);
							break;
							default:
								$g8_db_param=$g8_param_type.''.strtolower($g8_param);
								
							break;
					}
								
									if($g8_value=="" || $g8_value==" " || $g8_value==";" || $g8_value=="/n" || $g8_value==":"){
									$g8_value=0;
									}
									if($g8_time!=""){
										$g8_time_stamp=$g8_time;
									}else{
										$g8_time_stamp=date('Y-m-d H:i:s');
									}       				        
									
										//Function to insert values in DB in param_log table							
										if($g8_db_param!="g8_" && $g8_db_param!='T' && $g8_db_param!='' && $g8_db_param!="g8_device_" && $g8_db_param!=(is_numeric(substr($g8_db_param,4,10 )))){
									
												$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');
																				
										}	
		}
	$g8_db_param='g8_url';
	$g8_value=$g8_url;
	$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp,$g8_table='g8_param_log');				
	if($g8_lat!='' && $g8_long!='' && $g8_time_stamp!='0000-00-00 00:00:00'){
		$g8_insrt_track=g8_gpsg_track($g8_lo_id,$g8_lat,$g8_long,$g8_speed,$g8_dir,$g8_time_stamp);
	}
}
/**
 * Function to add alarm msg values in g8_gpsg_tack
 * 
 * @Author Prithviraj Singh Bisht (psbisht@g8.net)
 * 
 * @Date 14-Sep-2015
 * @subpackage GPSP
 * 
 * @param integer $g8_lat
 * @param integer $g8_long
 * @param integer $g8_lo_id
 * @param integer $g8_speed 
 * @param integer $g8_dir
 * @param string $g8_time_stamp
 * 
 * @return $g8_ret_val
 */ 

function g8_gpsg_track($g8_lo_id,$g8_lat,$g8_long,$g8_speed,$g8_dir,$g8_time_stamp){
	# function to update the g8_gpsg_tracker, 
	# The purpose of this function is to simply insert a new record into the location tracker table, 
	
	$g8_sql="
		SELECT
			*
		FROM
			`g8_gpsg_track`
		WHERE
			`g8_lo_id`='".$g8_lo_id."' 
			AND
			 `g8_lat`='".$g8_lat."' 
			AND
			 `g8_long`='".$g8_long."' 
			AND
			 `g8_speed`='".$g8_speed."' 
			AND
			 `g8_dev_timestamp`='".$g8_time_stamp."';
			";
#			echo "<br>#".$g8_sql."#<br>";
			$g8_result=mysql_query($g8_sql,$_SESSION['g8_link']);
			$g8_records_present = mysql_num_rows($g8_result);
#			echo "Records -->".$g8_records_present."<br>";
	if($g8_records_present == 0 && $g8_time_stamp!='0000-00-00 00:00:00') {
		$g8_sql = "INSERT 
				INTO 
					`g8_core`.`g8_gpsg_track` 
				
					(`g8_lo_id`,`g8_timestamp`,`g8_lat`, `g8_long`, `g8_speed`, `g8_dirn`, `g8_dev_timestamp`) 
				
				VALUES 
								('".$g8_lo_id."','".date('Y-m-d H:i:s')."', '".$g8_lat."', '".$g8_long."', '".$g8_speed."', '".$g8_dir."', '".$g8_time_stamp."');
					";
	#	echo "<br>##".$g8_sql."<br>";
		
			$g8_ret_val=mysql_query($g8_sql);	
		
		# Inserted
	}
}

function g8_default($g8_m,$g8_lo_id,$g8_param_type){
$g8_db_param=$g8_param_type;
$g8_value=$g8_m;
$g8_insrt=g8_update_poll_log($g8_lo_id,$g8_db_param,$g8_value,$g8_time_stamp=date('Y:m:d h:i:s'),$g8_table='g8_param_log');	
}
?>
